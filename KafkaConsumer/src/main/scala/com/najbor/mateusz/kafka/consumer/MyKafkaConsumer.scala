package com.najbor.mateusz.kafka.consumer

import java.util.Properties

import com.najbor.mateusz.kafka.core.config.KafkaProperties
import org.apache.kafka.clients.consumer.KafkaConsumer

import scala.collection.JavaConverters._

class MyKafkaConsumer(topic: String) {

  def runConsumerWithDefaultProperties(): Unit = {
    val properties = prepareProperties()
    runConsumer(properties)
  }

  def runConsumer(properties: Properties): Unit = {
    val consumer = new KafkaConsumer[String, String](properties)
    try {
      consume(consumer)
    } catch {
      case e: Exception => e.printStackTrace()
    } finally {
      consumer.close()
    }
  }

  private def consume(consumer: KafkaConsumer[String, String]): Unit = { //konsumuje wiadomości wysłane po utworzeniu consumera
    consumer.subscribe(List(topic).asJava)
    while (true) {
      val records = consumer.poll(10)
      for (record <- records.asScala) {
        println("test")
        println(record.value)
      }
    }
  }

  private def prepareProperties() = {
    val properties: Properties = new Properties
    properties.put("group.id", "MyConsumer")
    properties.put("bootstrap.servers", KafkaProperties.bootstrapServers)
    properties.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer")
    properties.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer")
    properties.put("enable.auto.commit", "true")
    properties.put("auto.commit.interval.ms", "1000")
    properties
  }
}
