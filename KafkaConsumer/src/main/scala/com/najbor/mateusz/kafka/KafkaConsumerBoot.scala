package com.najbor.mateusz.kafka

import com.najbor.mateusz.kafka.consumer.MyKafkaConsumer
import com.najbor.mateusz.kafka.core.config.KafkaProperties

object KafkaConsumerBoot extends App {
  println("Hello Kafka consumer")

  new MyKafkaConsumer(KafkaProperties.kafkaTopic).runConsumerWithDefaultProperties()
}
