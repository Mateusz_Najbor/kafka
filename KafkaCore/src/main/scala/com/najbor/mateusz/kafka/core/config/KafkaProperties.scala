package com.najbor.mateusz.kafka.core.config

import com.typesafe.config.{Config, ConfigFactory}

object KafkaProperties {

  private val config: Config = ConfigFactory.load("kafka.properties")

  lazy val kafkaTopic: String = config.getString("kafka.topic")
  lazy val bootstrapServers: String = config.getString("bootstrap.servers")
}
