package com.najbor.mateusz.kafka.producer

import java.util.Properties

import akka.actor.{Actor, PoisonPill}
import com.najbor.mateusz.kafka.core.config.KafkaProperties
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}

class KafkaProducerActor(topic: String) extends Actor {

  def runProducerWithDefaultProperties(): Unit = {
    val properties = prepareProperties()
    runProducer(properties)
  }

  def runProducer(properties: Properties): Unit = {
    val producer = new KafkaProducer[String, String](properties)

    println("Starting kafka producer")
    try {
      for (i <- 0 to 10) {
        val message = "Test message number " + i
        val record = new ProducerRecord[String, String](topic, message)
        producer.send(record)
        println("Sending message: " + message)
      }
      producer.flush()
    } catch {
      case e: Exception => e.printStackTrace()
    } finally {
      producer.close()
      context.self ! PoisonPill
    }
    context.self ! PoisonPill
  }

  private def prepareProperties() = {
    val properties: Properties = new Properties
    properties.put("bootstrap.servers", KafkaProperties.bootstrapServers)
    properties.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    properties.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    properties.put("acks", "all")
    properties
  }

  override def receive: Receive = {
    case Run =>
      runProducerWithDefaultProperties()
  }

  override def postStop(): Unit = {
    super.postStop()
    println("Stopping kafka producer actor")
  }
}

case object Run
