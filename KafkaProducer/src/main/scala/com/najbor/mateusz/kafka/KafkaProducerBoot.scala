package com.najbor.mateusz.kafka

import akka.actor.{ActorSystem, Props}
import com.najbor.mateusz.kafka.core.config.KafkaProperties
import com.najbor.mateusz.kafka.producer.{KafkaProducerActor, Run}

object KafkaProducerBoot extends App {
  println("Hello Kafka producer")

  val system = ActorSystem("HelloSystem")
  val props = Props(classOf[KafkaProducerActor], KafkaProperties.kafkaTopic)
  val producerActor = system.actorOf(props)

  producerActor ! Run
}
